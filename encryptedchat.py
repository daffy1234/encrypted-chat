from nacl.signing import SigningKey, VerifyKey
from nacl.public import PrivateKey, PublicKey, Box
from nacl.secret import SecretBox
from nacl.encoding import Base64Encoder
from nacl.pwhash import argon2i
from base64 import b64encode, b64decode
import os
import hashlib
from getpass import getpass
import json
import hmac
import pyperclip

def kdf(password, salt):
    return argon2i.kdf(SecretBox.KEY_SIZE, password, salt,
                       argon2i.OPSLIMIT_SENSITIVE,
                       argon2i.MEMLIMIT_SENSITIVE)

def packkey(key):
    checksum = hashlib.sha512(key).digest()[:4]
    package = b64encode(checksum + key)
    return package.decode()

def unpackkey(package):
    package = b64decode(package.encode())
    checksum = package[:4]
    key = package[4:]
    assert hashlib.sha512(key).digest()[:4] == checksum
    return key

if __name__ == '__main__':
    try:
        with open('publickeys.json', 'r') as f:
            publickeys = json.loads(f.read())
    except:
        publickeys = {}
    
    try:
        while True:
            with open('privatekey.key', 'rb') as f:
                salt, encryptedkey = [f.read(x) for x in [argon2i.SALTBYTES, -1]]
                password = getpass().encode()
                boxkey = kdf(password, salt)
                box = SecretBox(boxkey)
                try:
                    signer = SigningKey(box.decrypt(encryptedkey))
                    break
                except Exception as e:
                    print('Couldn\'t decrypt private key. '
                          'Delete it to make a new one')
    except Exception as e:
        salt = os.urandom(argon2i.SALTBYTES)
        while True:
            password = getpass().encode()
            confpassword = getpass('Confirm: ').encode()
            if password != confpassword:
                print('Confirmation failed')
                continue
            break
        signer = SigningKey.generate()
        boxkey = kdf(password, salt)
        box = SecretBox(boxkey)
        with open('privatekey.key', 'wb') as f:
            f.write(salt)
            f.write(box.encrypt(signer.encode()))
    print('My public key: {}'.format(packkey(signer.verify_key.encode())))
    pyperclip.copy(packkey(signer.verify_key.encode()))
    
    while True:
        partnername = input('Partner\'s name or public key: ')
        try:
            partnerpubkey = unpackkey(partnername)
            partnernickname = input('Enter a nickname for this key: ').lower()
            publickeys[partnernickname] = packkey(partnerpubkey)
            with open('publickeys.json', 'w') as f:
                f.write(json.dumps(publickeys))
            break
        except Exception as e:
            partnername = partnername.lower()
            if partnername in publickeys:
                partnerpubkey = unpackkey(publickeys[partnername])
                break
            else:
                print('Public key or nickname not understood')
    verifier = VerifyKey(partnerpubkey)

    ephempriv = PrivateKey.generate()
    ephempub = ephempriv.public_key
    signedpub = signer.sign(ephempub.encode())

    print('Ephemeral key:\n{}'.format(packkey(signedpub)))
    pyperclip.copy(packkey(signedpub))
    while True:
        try:
            partnersignedkey = unpackkey(input('Partner\'s ephemeral key: '))
            partnerkey = verifier.verify(partnersignedkey)
            partnerephem = PublicKey(partnerkey)
            break
        except:
            print('Ephemeral key could not be verified')

    sharedkey = Box(ephempriv, partnerephem).shared_key()

    mykey = hmac.HMAC(sharedkey, signer.verify_key.encode(), 'sha256').digest()
    partnerkey = hmac.HMAC(sharedkey, verifier.encode(), 'sha256').digest()

    mybox = SecretBox(mykey)
    partnerbox = SecretBox(partnerkey)
    
    while True:
        message = input('> ')
        if message.startswith('m:'):
            try:
                message = partnerbox.decrypt(message.encode()[2:],
                                             encoder=Base64Encoder)
                print('Partner: '+message.decode())
            except Exception as e:
                print('Error decrypting: ' + str(e))
        elif message == '':
            # it was just an accidental enter press, ignore it
            pass
        else:
            line = input('. ')
            while line:
                message += '\n' + line
                line = input('. ')
            try:
                message = mybox.encrypt(message.encode(), encoder=Base64Encoder)
                print('m:'+message.decode())
                pyperclip.copy('m:'+message.decode())
            except Exception as e:
                print('Error encrypting: ' + str(e))
